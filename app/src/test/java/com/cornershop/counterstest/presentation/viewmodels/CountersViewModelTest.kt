package com.cornershop.counterstest.presentation.viewmodels

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import androidx.work.WorkManager
import com.cornershop.counterstest.data.Response
import com.cornershop.counterstest.data.mappers.toSelectableCounterList
import com.cornershop.counterstest.domain.response.SelectableCounter
import com.cornershop.counterstest.testShared.mockedCounter
import com.cornershop.counterstest.testShared.mockedSelectableCounter
import com.cornershop.counterstest.usecases.usecases.local.*
import com.cornershop.counterstest.usecases.usecases.server.RemoteGetCountersUseCase
import com.cornershop.counterstest.usecases.usecases.server.RemoteSaveCounterUseCase
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class CountersViewModelTest {
    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    lateinit var remoteSaveCounterUseCase: RemoteSaveCounterUseCase

    @Mock
    lateinit var remoteGetCountersUseCase: RemoteGetCountersUseCase

    @Mock
    lateinit var localDecrementCounterUseCase: LocalDecrementCounterUseCase

    @Mock
    lateinit var localDeleteCounterUseCase: LocalDeleteCounterUseCase

    @Mock
    lateinit var localGetCountersUseCase: LocalGetCountersUseCase

    @Mock
    lateinit var localIncrementCounterUseCase: LocalIncrementCounterUseCase

    @Mock
    lateinit var localSaveCounterUseCase: LocalSaveCounterUseCase

    @Mock
    lateinit var workManager: WorkManager

    @Mock
    lateinit var observer: Observer<Response<Pair<Int, List<SelectableCounter>>>>

    private lateinit var vm: CountersViewModel

    @Before
    fun setUp() {
        vm = CountersViewModel(
            remoteSaveCounterUseCase,
            remoteGetCountersUseCase,
            localDecrementCounterUseCase,
            localDeleteCounterUseCase,
            localGetCountersUseCase,
            localIncrementCounterUseCase,
            localSaveCounterUseCase,
            workManager
        )
    }

    @Test
    fun `observing LiveData increments`() {
        runBlocking {
            val counter = mockedCounter.copy(count = 1)
            whenever(localIncrementCounterUseCase.invoke(mockedCounter)).thenReturn(listOf(counter))
            whenever(localGetCountersUseCase.invoke()).thenReturn(listOf(counter))
            vm.liveData.observeForever(observer)
            vm.incrementCounter(mockedCounter)
            verify(localIncrementCounterUseCase).invoke(mockedCounter)
            verify(observer).onChanged(Response.Loading())
            verify(observer).onChanged(Response.Success(1 to listOf(counter).toSelectableCounterList()))
        }
    }

    @Test
    fun `observing LiveData decrements`() {
        runBlocking {
            val counter = mockedCounter.copy(count = 1)
            val resultCounter = mockedCounter.copy(count = 0)
            whenever(localDecrementCounterUseCase.invoke(counter))
                .thenReturn(listOf(resultCounter))
            whenever(localGetCountersUseCase.invoke()).thenReturn(listOf(resultCounter))
            vm.liveData.observeForever(observer)
            vm.decrementCounter(counter)
            verify(localDecrementCounterUseCase).invoke(counter)
            verify(observer).onChanged(Response.Loading())
            verify(observer).onChanged(Response.Success(0 to listOf(resultCounter).toSelectableCounterList()))
        }
    }

    @Test
    fun `observing LiveData, deletes Counter`() {
        runBlocking {
            whenever(localDeleteCounterUseCase.invoke(mockedCounter)).thenReturn(listOf())
            vm.liveData.observeForever(observer)
            vm.delete(listOf(mockedSelectableCounter))
            verify(localDeleteCounterUseCase).invoke(mockedCounter)
        }
    }

    @Test
    fun `observing LiveData, saves Counter into the server`() {
        runBlocking {
            val counter = mockedCounter.copy(id = "1", title = "Test", count = 1)
            whenever(localSaveCounterUseCase.invoke(mockedCounter)).thenReturn(listOf(counter))
            vm.liveData.observeForever(observer)
            vm.saveCounter("Title")
            verify(remoteSaveCounterUseCase).invoke("Title")
        }
    }
}