package com.cornershop.counterstest.data.worker

import android.content.Context
import android.util.Log
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.work.Configuration
import androidx.work.WorkInfo
import androidx.work.WorkManager
import androidx.work.testing.SynchronousExecutor
import androidx.work.testing.WorkManagerTestInitHelper
import androidx.work.workDataOf
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class SyncDataWorkerTest {

    private lateinit var context: Context

    @Before
    fun setup() {
        context = ApplicationProvider.getApplicationContext()
        val config = Configuration.Builder()
            .setMinimumLoggingLevel(Log.DEBUG)
            .setExecutor(SynchronousExecutor())
            .build()
        WorkManagerTestInitHelper.initializeTestWorkManager(context, config)
    }

    @Test
    fun `test save Counter for SyncDataWorker jobs`() {
        val inputData = workDataOf(
            SyncDataWorker.SYNC_TASK to SyncDataWorker.SyncTask.SAVE_COUNTER.name,
            SyncDataWorker.TITLE to "Title"
        )
        val request = SyncDataWorker.createRequest(inputData)
        val workManager = WorkManager.getInstance(context)
        workManager.enqueue(request).result.get()
        val workInfo = workManager.getWorkInfoById(request.id).get()
        assertThat(workInfo.state, `is`(WorkInfo.State.ENQUEUED))
    }

    @Test
    fun `test delete Counter for SyncDataWorker jobs`() {
        val inputData = workDataOf(
            SyncDataWorker.SYNC_TASK to SyncDataWorker.SyncTask.DELETE_COUNTER.name,
            SyncDataWorker.ID to "123"
        )
        val request = SyncDataWorker.createRequest(inputData)
        val workManager = WorkManager.getInstance(context)
        workManager.enqueue(request).result.get()
        val workInfo = workManager.getWorkInfoById(request.id).get()
        assertThat(workInfo.state, `is`(WorkInfo.State.ENQUEUED))
    }

    @Test
    fun `test increment Counter for SyncDataWorker jobs`() {
        val inputData = workDataOf(
            SyncDataWorker.SYNC_TASK to SyncDataWorker.SyncTask.INCREMENT_COUNTER.name,
            SyncDataWorker.ID to "123"
        )
        val request = SyncDataWorker.createRequest(inputData)
        val workManager = WorkManager.getInstance(context)
        workManager.enqueue(request).result.get()
        val workInfo = workManager.getWorkInfoById(request.id).get()
        assertThat(workInfo.state, `is`(WorkInfo.State.ENQUEUED))
    }

    @Test
    fun `test decrement Counter for SyncDataWorker jobs`() {
        val inputData = workDataOf(
            SyncDataWorker.SYNC_TASK to SyncDataWorker.SyncTask.DECREMENT_COUNTER.name,
            SyncDataWorker.ID to "123"
        )
        val request = SyncDataWorker.createRequest(inputData)
        val workManager = WorkManager.getInstance(context)
        workManager.enqueue(request).result.get()
        val workInfo = workManager.getWorkInfoById(request.id).get()
        assertThat(workInfo.state, `is`(WorkInfo.State.ENQUEUED))
    }
}