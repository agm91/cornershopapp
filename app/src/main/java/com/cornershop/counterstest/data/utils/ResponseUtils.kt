package com.cornershop.counterstest.data.utils

import android.view.View
import android.widget.ProgressBar
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.cornershop.counterstest.data.Response
import com.cornershop.counterstest.presentation.common.gone
import com.cornershop.counterstest.presentation.common.show
import com.cornershop.counterstest.presentation.common.showSnackBar

fun <T> Response<T>.handleResponse(
    progressLayout: ProgressBar? = null,
    swipeRefreshLayout: SwipeRefreshLayout? = null,
    whenSuccess: (response: T) -> Unit,
    whenError: (messageHandler: MessageHandler) -> Unit
) {
    when (this) {
        is Response.Error -> {
            progressLayout?.gone()
            swipeRefreshLayout?.isRefreshing = false
            whenError.invoke(this.error)
        }
        is Response.Loading -> {
            progressLayout?.show()
            swipeRefreshLayout?.isRefreshing = true
        }
        is Response.Success -> {
            progressLayout?.gone()
            swipeRefreshLayout?.isRefreshing = false
            whenSuccess.invoke(this.data)
        }
    }
}

fun <T> Response<T>.handleResponse(
    view: View,
    progressLayout: ProgressBar? = null,
    swipeRefreshLayout: SwipeRefreshLayout? = null,
    whenSuccess: (T) -> Unit
) {
    when (this) {
        is Response.Error -> {
            progressLayout?.gone()
            swipeRefreshLayout?.isRefreshing = false
            view.showSnackBar(this.error.getMessage(view.context))
        }
        is Response.Loading -> {
            progressLayout?.show()
            swipeRefreshLayout?.isRefreshing = true
        }
        is Response.Success -> {
            progressLayout?.gone()
            swipeRefreshLayout?.isRefreshing = false
            whenSuccess.invoke(this.data)
        }
    }
}