package com.cornershop.counterstest.data.server

import com.cornershop.counterstest.domain.entity.IdEntity
import com.cornershop.counterstest.domain.entity.TitleEntity
import retrofit2.http.*

interface CounterApi {
    @GET("api/v1/counters")
    suspend fun getCounters(): List<CounterResponse>

    @POST("api/v1/counter")
    suspend fun saveCounter(@Body body: TitleEntity): List<CounterResponse>

    @POST("api/v1/counter/inc")
    suspend fun incrementCounter(@Body body: IdEntity): List<CounterResponse>

    @POST("api/v1/counter/dec")
    suspend fun decrementCounter(@Body body: IdEntity): List<CounterResponse>

    @HTTP(method = "DELETE", path = "api/v1/counter", hasBody = true)
    suspend fun deleteCounter(@Body body: IdEntity): List<CounterResponse>
}