package com.cornershop.counterstest.data.database

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "counters")
data class CounterDatabaseEntity(
    @PrimaryKey(autoGenerate = false)
    val roomId: Int,
    val id: String,
    val title: String,
    val count: Int
)