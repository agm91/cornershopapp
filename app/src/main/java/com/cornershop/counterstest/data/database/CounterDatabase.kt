package com.cornershop.counterstest.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [CounterDatabaseEntity::class], version = 1, exportSchema = false)
abstract class CounterDatabase : RoomDatabase() {
    companion object {
        @Volatile
        private var instance: CounterDatabase? = null

        private fun build(context: Context) = Room.databaseBuilder(
            context, CounterDatabase::class.java, "counter-db"
        ).build()

        fun getDatabase(context: Context): CounterDatabase =
            instance ?: synchronized(this) {
                instance ?: build(context).also {
                    instance = it
                }
            }
    }

    abstract fun counterDao(): CounterDao
}