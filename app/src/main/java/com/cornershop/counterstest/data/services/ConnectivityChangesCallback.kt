package com.cornershop.counterstest.data.services

interface ConnectivityChangesCallback {
    fun onNetworkChanged(isConnected: Boolean)
}