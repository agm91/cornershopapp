package com.cornershop.counterstest.data.database

import androidx.room.*

@Dao
interface CounterDao {
    @Query("SELECT * FROM counters")
    suspend fun getCounters(): List<CounterDatabaseEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun saveCounter(counter: CounterDatabaseEntity)

    @Query("SELECT * FROM counters WHERE id = :id")
    suspend fun findById(id: String): CounterDatabaseEntity

    @Delete
    suspend fun deleteCounter(counter: CounterDatabaseEntity)
}