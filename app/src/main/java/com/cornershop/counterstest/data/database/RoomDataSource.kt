package com.cornershop.counterstest.data.database

import com.cornershop.counterstest.data.mappers.decrement
import com.cornershop.counterstest.data.mappers.increment
import com.cornershop.counterstest.data.mappers.toDomainCounter
import com.cornershop.counterstest.data.mappers.toRoomCounter
import com.cornershop.counterstest.data.source.LocalDataSource
import com.cornershop.counterstest.domain.response.Counter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.*

class RoomDataSource(db: CounterDatabase) : LocalDataSource {
    private val counterDb = db.counterDao()

    override suspend fun getCounters(): List<Counter> {
        return withContext(Dispatchers.IO) {
            return@withContext counterDb.getCounters().map { it.toDomainCounter() }
        }
    }

    override suspend fun saveCounter(counter: Counter): List<Counter> {
        return withContext(Dispatchers.IO) {
            counterDb.saveCounter(counter.toRoomCounter())
            return@withContext counterDb.getCounters().map { it.toDomainCounter() }
        }
    }

    override suspend fun incrementCounter(counter: Counter): List<Counter> {
        return withContext(Dispatchers.IO) {
            val localCounter = counterDb.findById(counter.id)
            counterDb.saveCounter(localCounter.increment())
            return@withContext getCounters()
        }
    }

    override suspend fun counterCount(): Int {
        return withContext(Dispatchers.IO) {
            return@withContext counterDb.getCounters().size
        }
    }

    override suspend fun decrementCounter(counter: Counter): List<Counter> {
        return withContext(Dispatchers.IO) {
            val localCounter = counterDb.findById(counter.id)
            counterDb.saveCounter(localCounter.decrement())
            return@withContext getCounters()
        }
    }

    override suspend fun deleteCounter(counter: Counter): List<Counter> {
        return withContext(Dispatchers.IO) {
            counterDb.deleteCounter(counter.toRoomCounter())
            return@withContext counterDb.getCounters().map { it.toDomainCounter() }
        }
    }
}