package com.cornershop.counterstest.data.mappers

import com.cornershop.counterstest.data.database.CounterDatabaseEntity
import com.cornershop.counterstest.data.server.CounterResponse
import com.cornershop.counterstest.domain.response.Counter
import com.cornershop.counterstest.domain.response.SelectableCounter
import java.util.*

fun CounterDatabaseEntity.toDomainCounter(): Counter =
    Counter(this.id, this.title, this.count)

fun CounterDatabaseEntity.increment(): CounterDatabaseEntity =
    CounterDatabaseEntity(this.roomId, this.id, this.title, this.count + 1)

fun CounterDatabaseEntity.decrement(): CounterDatabaseEntity =
    CounterDatabaseEntity(this.roomId, this.id, this.title, this.count - 1)

fun Counter.toRoomCounter(): CounterDatabaseEntity =
    CounterDatabaseEntity(
        UUID.nameUUIDFromBytes(this.id.toByteArray()).mostSignificantBits.toInt(),
        this.id, this.title, this.count
    )

fun CounterResponse.toDomainCounter(): Counter =
    Counter(this.id, this.title, this.count)

fun List<Counter>.toSelectableCounterList() =
    this.map { SelectableCounter(false, it.id, it.title, it.count) }

fun List<SelectableCounter>.toCounterList() = this.map { Counter(it.id, it.title, it.count) }

fun SelectableCounter.toCounter() = Counter(this.id, this.title, this.count)

fun Collection<SelectableCounter?>.toCounterString(): String {
    return this.mapIndexed { index, counter ->
        " ${index + 1}.- ${counter?.title}: ${counter?.count}"
    }.toString()
        .replace(", ", "\n")
        .replace("[", "")
        .replace("]", "")
}