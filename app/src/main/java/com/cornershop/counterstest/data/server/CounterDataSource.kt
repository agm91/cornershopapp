package com.cornershop.counterstest.data.server

import com.cornershop.counterstest.data.mappers.toDomainCounter
import com.cornershop.counterstest.data.source.RemoteDataSource
import com.cornershop.counterstest.domain.entity.IdEntity
import com.cornershop.counterstest.domain.entity.TitleEntity

class CounterDataSource(private val service: CounterApi) : RemoteDataSource {
    override suspend fun getCounters() =
        service.getCounters().map { it.toDomainCounter() }

    override suspend fun saveCounter(title: String) =
        service.saveCounter(TitleEntity(title)).map { it.toDomainCounter() }

    override suspend fun incrementCounter(id: String) =
        service.incrementCounter(IdEntity(id)).map { it.toDomainCounter() }

    override suspend fun decrementCounter(id: String) =
        service.decrementCounter(IdEntity(id)).map { it.toDomainCounter() }

    override suspend fun deleteCounter(id: String) =
        service.deleteCounter(IdEntity(id)).map { it.toDomainCounter() }
}