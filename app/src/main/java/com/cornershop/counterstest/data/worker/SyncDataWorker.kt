package com.cornershop.counterstest.data.worker

import android.content.Context
import androidx.hilt.work.HiltWorker
import androidx.work.*
import com.cornershop.counterstest.data.repository.RemoteCounterRepository
import com.cornershop.counterstest.data.utils.safeValueOf
import com.cornershop.counterstest.data.worker.SyncDataWorker.SyncTask.*
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.withContext

@HiltWorker
class SyncDataWorker @AssistedInject constructor(
    @Assisted context: Context,
    @Assisted workerParameters: WorkerParameters,
    private val remoteCounterRepository: RemoteCounterRepository
) : CoroutineWorker(context, workerParameters) {
    override suspend fun doWork(): Result {
        return withContext(IO) {
            when (safeValueOf<SyncTask>(inputData.getString(SYNC_TASK))) {
                GET_COUNTERS -> remoteCounterRepository.getCounters()
                SAVE_COUNTER ->
                    inputData.getString(TITLE)?.let {
                        remoteCounterRepository.saveCounter(it)
                    }
                INCREMENT_COUNTER ->
                    inputData.getString(ID)?.let {
                        remoteCounterRepository.incrementCounter(it)
                    }
                DECREMENT_COUNTER ->
                    inputData.getString(ID)?.let {
                        remoteCounterRepository.decrementCounter(it)
                    }
                DELETE_COUNTER -> inputData.getString(ID)?.let {
                    remoteCounterRepository.deleteCounter(it)
                }
                UNKNOWN, null -> return@withContext Result.failure()
            }
            return@withContext Result.success()
        }
    }

    companion object {
        //name of the group of Jobs
        const val JOB_GROUP_NAME = "jobs"

        //for getting the current task that worker needs to do -> SyncTask
        const val SYNC_TASK = "sync_task"

        //for creating new counter
        const val TITLE = "title"

        //for getting the result back
        const val RESULT = "result"

        //for incrementing, decreasing or deleting counter
        const val ID = "id"

        //for getting the error if any
        const val ERROR = "error"

        fun createRequest(data: Data): WorkRequest {
            val networkConstraint = Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .build()
            return OneTimeWorkRequestBuilder<SyncDataWorker>()
                .setConstraints(networkConstraint)
                .setInputData(data)
                .build()
        }
    }

    enum class SyncTask {
        GET_COUNTERS, SAVE_COUNTER, INCREMENT_COUNTER, DECREMENT_COUNTER, DELETE_COUNTER, UNKNOWN
    }
}