package com.cornershop.counterstest.data

import com.cornershop.counterstest.data.utils.MessageHandler

sealed class Response<out T> {
    data class Success<out T>(val data: T) : Response<T>()
    data class Error<out T>(val error: MessageHandler, val data: T? = null) : Response<T>()
    data class Loading<out T>(private val isLoading: Boolean = true) : Response<T>()
}