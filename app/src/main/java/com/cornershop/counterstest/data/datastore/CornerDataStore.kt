package com.cornershop.counterstest.data.datastore

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.preferencesKey
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class CornerDataStore @Inject constructor(
    private val dataStore: DataStore<Preferences>
) {
    suspend fun saveToDataStore(hasShownWelcome: Boolean) {
        dataStore.edit { preferences ->
            preferences[HAS_SHOWN_WELCOME] = hasShownWelcome
        }
    }

    val hasShownWelcome: Flow<Boolean> = dataStore.data
        .map { preferences ->
            preferences[HAS_SHOWN_WELCOME] ?: false
        }

    companion object {
        private val HAS_SHOWN_WELCOME = preferencesKey<Boolean>("hasShownWelcome")
        const val NAME = "corner_datastore"
    }
}