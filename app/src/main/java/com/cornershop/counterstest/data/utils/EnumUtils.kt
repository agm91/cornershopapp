package com.cornershop.counterstest.data.utils

inline fun <reified T : Enum<T>> safeValueOf(type: String?): T? {
    return if (type == null) null else java.lang.Enum.valueOf(T::class.java, type)
}