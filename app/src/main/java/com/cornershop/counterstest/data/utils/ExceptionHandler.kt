package com.cornershop.counterstest.data.utils

import android.content.Context
import com.cornershop.counterstest.R
import com.google.android.gms.common.api.ApiException
import retrofit2.HttpException
import java.io.IOException

private const val HTTP_CODE_400_START = 400
private const val HTTP_CODE_400_END = 499
private const val HTTP_CODE_500_START = 500
private const val HTTP_CODE_500_END = 599

fun Throwable.getPersonalizedMessage(): MessageHandler {
    return when (this) {
        is HttpException -> {
            this.getMessage()
        }
        is IOException -> {
            MessageHandler(R.string.error_server_message, "")
        }
        is Thread.UncaughtExceptionHandler -> {
            MessageHandler(R.string.error_unexpected_message, "")
        }
        is ApiException -> {
            MessageHandler(R.string.connection_error_description, "")
        }
        else -> {
            MessageHandler(R.string.error_unexpected_message, "")
        }
    }
}

fun HttpException.getMessage(): MessageHandler {
    return when (this.code()) {
        in HTTP_CODE_400_START..HTTP_CODE_400_END -> {
            MessageHandler(R.string.error_client_message, this.code().toString())
        }
        in HTTP_CODE_500_START..HTTP_CODE_500_END -> {
            MessageHandler(null, "")
        }
        else -> {
            MessageHandler(R.string.error_unexpected_message, this.code().toString())
        }
    }
}

data class MessageHandler(val stringResource: Int? = null, val codeOrMessage: String? = null)

fun MessageHandler.getMessage(context: Context): String =
    if (stringResource != null) context.getString(stringResource) else codeOrMessage ?: ""