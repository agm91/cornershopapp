package com.cornershop.counterstest.di

import com.cornershop.counterstest.data.repository.LocalCounterRepository
import com.cornershop.counterstest.data.repository.RemoteCounterRepository
import com.cornershop.counterstest.usecases.usecases.local.*
import com.cornershop.counterstest.usecases.usecases.server.RemoteGetCountersUseCase
import com.cornershop.counterstest.usecases.usecases.server.RemoteSaveCounterUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class UseCaseModule {
    @Singleton
    @Provides
    fun provideRemoteSaveCounterUseCase(remoteCounterRepository: RemoteCounterRepository) =
        RemoteSaveCounterUseCase(remoteCounterRepository)

    @Singleton
    @Provides
    fun provideRemoteGetCountersUseCase(remoteCounterRepository: RemoteCounterRepository) =
        RemoteGetCountersUseCase(remoteCounterRepository)

    @Singleton
    @Provides
    fun provideLocalDecrementCounterUseCase(localCounterRepository: LocalCounterRepository) =
        LocalDecrementCounterUseCase(localCounterRepository)

    @Singleton
    @Provides
    fun provideLocalIncrementCounterUseCase(localCounterRepository: LocalCounterRepository) =
        LocalIncrementCounterUseCase(localCounterRepository)

    @Singleton
    @Provides
    fun provideLocalGetCountersUseCase(localCounterRepository: LocalCounterRepository) =
        LocalGetCountersUseCase(localCounterRepository)

    @Singleton
    @Provides
    fun provideLocalSaveCounterUseCase(localCounterRepository: LocalCounterRepository) =
        LocalSaveCounterUseCase(localCounterRepository)

    @Singleton
    @Provides
    fun provideLocalDeleteCounterUseCase(localCounterRepository: LocalCounterRepository) =
        LocalDeleteCounterUseCase(localCounterRepository)
}