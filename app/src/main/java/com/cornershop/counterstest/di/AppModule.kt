package com.cornershop.counterstest.di

import android.content.Context
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.createDataStore
import androidx.work.WorkManager
import com.cornershop.counterstest.BuildConfig
import com.cornershop.counterstest.data.database.CounterDatabase
import com.cornershop.counterstest.data.database.RoomDataSource
import com.cornershop.counterstest.data.datastore.CornerDataStore
import com.cornershop.counterstest.data.server.CounterApi
import com.cornershop.counterstest.data.server.CounterDataSource
import com.cornershop.counterstest.data.source.LocalDataSource
import com.cornershop.counterstest.data.source.RemoteDataSource
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class AppModule {
    @Provides
    fun provideBaseUrl() = BuildConfig.BASE_URL

    @Provides
    @Singleton
    fun provideOkHttpClient() = if (BuildConfig.DEBUG) {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .build()
    } else OkHttpClient
        .Builder()
        .build()

    @Provides
    fun provideGson(): Gson = GsonBuilder().create()

    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient, baseUrl: String): Retrofit =
        Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(baseUrl)
            .client(okHttpClient)
            .build()

    @Provides
    @Singleton
    fun provideApiService(retrofit: Retrofit): CounterApi = retrofit.create(CounterApi::class.java)

    @Singleton
    @Provides
    fun provideRemoteDataSource(counterApi: CounterApi): RemoteDataSource =
        CounterDataSource(counterApi)

    @Singleton
    @Provides
    fun provideDatabase(@ApplicationContext appContext: Context) =
        CounterDatabase.getDatabase(appContext)

    @Provides
    fun providesLocalDataSource(db: CounterDatabase): LocalDataSource = RoomDataSource(db)

    @Singleton
    @Provides
    fun provideCounterDao(db: CounterDatabase) = db.counterDao()

    @Singleton
    @Provides
    fun provideDataStore(@ApplicationContext appContext: Context): DataStore<Preferences> =
        appContext.createDataStore(name = CornerDataStore.NAME)

    @Singleton
    @Provides
    fun provideCornerDataStore(dataStore: DataStore<Preferences>) = CornerDataStore(dataStore)

    @Singleton
    @Provides
    fun providesWorkManager(@ApplicationContext appContext: Context): WorkManager =
        WorkManager.getInstance(appContext)

    @Singleton
    @Provides
    fun providesNetworkRequest(): NetworkRequest = NetworkRequest.Builder()
        .addTransportType(NetworkCapabilities.TRANSPORT_CELLULAR)
        .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
        .build()
}