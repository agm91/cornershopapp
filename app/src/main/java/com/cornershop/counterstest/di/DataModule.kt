package com.cornershop.counterstest.di

import com.cornershop.counterstest.data.repository.LocalCounterRepository
import com.cornershop.counterstest.data.repository.RemoteCounterRepository
import com.cornershop.counterstest.data.source.LocalDataSource
import com.cornershop.counterstest.data.source.RemoteDataSource
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DataModule {
    @Provides
    @Singleton
    fun provideRemoteCounterRepository(remoteDataSource: RemoteDataSource) =
        RemoteCounterRepository(remoteDataSource)

    @Provides
    @Singleton
    fun provideLocalCounterRepository(localDataSource: LocalDataSource) =
        LocalCounterRepository(localDataSource)
}