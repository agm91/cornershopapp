package com.cornershop.counterstest.presentation.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.NavHostFragment.findNavController
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.cornershop.counterstest.R
import com.cornershop.counterstest.data.mappers.toCounterString
import com.cornershop.counterstest.data.utils.handleResponse
import com.cornershop.counterstest.databinding.FragmentCountersBinding
import com.cornershop.counterstest.domain.response.Counter
import com.cornershop.counterstest.domain.response.SelectableCounter
import com.cornershop.counterstest.presentation.adapters.CountersAdapter
import com.cornershop.counterstest.presentation.common.*
import com.cornershop.counterstest.presentation.dialog.DeleteCountersDialog
import com.cornershop.counterstest.presentation.viewmodels.CountersViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class CountersFragment : Fragment(), CountersAdapter.OnCounterClickListener {
    private lateinit var binding: FragmentCountersBinding

    private val viewModel: CountersViewModel by activityViewModels()

    @Inject
    lateinit var adapter: CountersAdapter

    private var loading = LoadingEnum.PROGRESS_BAR
    private val searchTextWatcher = object : TextWatcher {
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) = Unit
        override fun afterTextChanged(p0: Editable?) = Unit
        override fun onTextChanged(text: CharSequence?, p1: Int, p2: Int, p3: Int) {
            if (text?.isEmpty() == true)
                binding.searchFrameLayout.show()
            else
                binding.searchFrameLayout.gone()
            adapter.filter.filter(text)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_counters, container, false
        )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        observeForCounters()
        observeForDialogDeleteClicked()
        viewModel.getCounters(true)
    }

    private fun observeForDialogDeleteClicked() {
        parentFragmentManager
            .setFragmentResultListener(ACTION_DELETE, viewLifecycleOwner) { requestKey, result ->
                if (result.getBoolean(DeleteCountersDialog.DELETE_SELECTED)) {
                    deleteCounters()
                }
            }
    }

    private fun observeForCounters() {
        viewModel.liveData.observe(viewLifecycleOwner, { response ->
            val (progress, swipe) = getProgressOrSwipe()
            response.handleResponse(
                binding.root, progressLayout = progress, swipeRefreshLayout = swipe
            ) { data ->
                populateViewsWithData(data)
            }
        })
    }

    private fun getProgressOrSwipe(): Pair<ProgressBar?, SwipeRefreshLayout?> {
        return if (loading == LoadingEnum.PROGRESS_BAR)
            binding.layoutProgress.progressBar to null
        else
            null to binding.swipeRefresh
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun initView() {
        with(binding) {
            buttonAddCounter.setOnClickListener { onAddCounterButtonClicked() }
            adapter.listener = this@CountersFragment
            recyclerView.adapter = adapter
            swipeRefresh.setOnRefreshListener {
                loading = LoadingEnum.SWIPE_REFRESH
                viewModel.getCounters(firstLoad = true)
            }
            searchView.setOnClickListener { showSearchViews() }
            searchLayout.setStartIconOnClickListener { hideSearchViews() }
            searchFrameLayout.setOnClickListener { hideSearchViews() }
            buttonToolbarSelectionAction.setOnClickListener { showDeleteCountersDialog() }
            buttonToolbarSelectionCancel.setOnClickListener {
                adapter.selectedMap.clear()
                val list = ArrayList(adapter.currentList)
                adapter.submitList(list.map { it.isSelected = false; it })
                adapter.notifyDataSetChanged()
                deactivateMultipleSelection()
            }
        }
    }

    private fun showSearchViews() {
        with(binding) {
            searchCardview.gone()
            searchLayout.show()
            searchFrameLayout.show()
            searchTextInputEdit.addTextChangedListener(searchTextWatcher)
        }
    }

    private fun hideSearchViews() {
        with(binding) {
            searchTextInputEdit.setText("")
            searchCardview.show()
            searchLayout.gone()
            searchTextInputEdit.removeTextChangedListener(searchTextWatcher)
            searchFrameLayout.gone()
            hideKeyboard()
        }
    }

    private fun populateViewsWithData(data: Pair<Int, List<SelectableCounter>>) {
        if (data.second.isNotEmpty()) {
            val selectableList = data.second
            showList()
            getCountersInfo(data.first, selectableList)
            adapter.list = selectableList.toMutableList()
            adapter.filter.filter(binding.searchTextInputEdit.text.toString())
            adapter.submitList(selectableList)
        } else {
            hideList()
        }
    }

    private fun showList() {
        binding.groupList.show()
        binding.containerEmpty.groupEmptyList.gone()
    }

    private fun hideList() {
        binding.groupList.gone()
        binding.containerEmpty.groupEmptyList.show()
    }

    private fun onAddCounterButtonClicked() = findNavController(this).navigateSafely(
        CountersFragmentDirections.actionCountersFragmentToAddCounterFragment()
    )

    override fun activateMultipleSelection() {
        binding.searchCardview.gone()
        binding.layoutDeleteToolbar.show()
    }

    override fun deactivateMultipleSelection() {
        binding.searchCardview.show()
        binding.layoutDeleteToolbar.gone()
    }

    override fun getCountersInfo(times: Int?, list: List<SelectableCounter>?) {
        if (list?.size == 0) {
            showNoResults()
        } else {
            hideNoResults()
            binding.textviewTimes.text = getString(R.string.n_times, times)
            binding.textviewItems.text = getString(R.string.n_items, list?.size)
        }
    }

    private fun hideNoResults() {
        binding.searchFrameNoResultsLayout?.gone()
        binding.textviewTimes.show()
        binding.textviewItems.show()
    }

    private fun showNoResults() {
        binding.searchFrameNoResultsLayout?.show()
        binding.textviewTimes.gone()
        binding.textviewItems.gone()
    }


    override fun selected(amount: Int) {
        binding.textviewToolbarSelectionTitle.text = getString(R.string.n_selected, amount)
    }

    override fun onIncrement(counter: Counter) = viewModel.incrementCounter(counter)
    override fun onDecrement(counter: Counter) = viewModel.decrementCounter(counter)

    private fun showDeleteCountersDialog() {
        DeleteCountersDialog.newInstance(
            getString(R.string.delete_x_question),
            adapter.selectedMap.values.toCounterString(),
            getString(R.string.delete),
            getString(R.string.dismiss)
        ).show(parentFragmentManager, "dialog_delete")
    }

    private fun deleteCounters() {
        deactivateMultipleSelection()
        viewModel.delete(
            (adapter.selectedMap.values.toList() as? List<SelectableCounter>)
        )
        adapter.selectedMap.clear()
    }

    companion object {
        const val ACTION_DELETE = "action_delete"

        @JvmStatic
        fun newInstance() = CountersFragment()
    }
}