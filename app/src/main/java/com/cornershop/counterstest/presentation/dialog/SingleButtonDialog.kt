package com.cornershop.counterstest.presentation.dialog

import android.app.AlertDialog
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.cornershop.counterstest.R
import com.cornershop.counterstest.databinding.DialogTwoButtonsBinding

class SingleButtonDialog : DialogFragment() {
    private lateinit var binding: DialogTwoButtonsBinding

    private var title: String? = null
    private var description: String? = null
    private var buttonText: String? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = AlertDialog.Builder(requireActivity())
        dialog.setCancelable(false)
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.dialog_two_buttons,
            null,
            false
        )
        initDialog()
        dialog.setView(binding.root)
        this@SingleButtonDialog.dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        return dialog.create()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
        return binding.root
    }

    private fun initDialog() {
        with(binding) {
            textViewTitle.text = title
            textViewContent.text = description
            textViewActionPositive.text = buttonText
            textViewActionPositive.setOnClickListener { this@SingleButtonDialog.dismiss() }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        title = arguments?.getString(TITLE)
        description = arguments?.getString(DESCRIPTION)
        buttonText = arguments?.getString(BUTTON_TEXT)
    }

    companion object {
        private const val TITLE = "ip"
        private const val DESCRIPTION = "description"
        private const val BUTTON_TEXT = "button_text"

        @JvmStatic
        fun newInstance(
            title: String, description: String, buttonText: String
        ): SingleButtonDialog {
            val fragment = SingleButtonDialog()
            val args = Bundle().apply {
                putString(TITLE, title)
                putString(DESCRIPTION, description)
                putString(BUTTON_TEXT, buttonText)
            }
            fragment.arguments = args
            return fragment
        }
    }
}