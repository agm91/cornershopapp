package com.cornershop.counterstest.presentation.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import androidx.work.workDataOf
import com.cornershop.counterstest.data.Response
import com.cornershop.counterstest.data.mappers.toCounter
import com.cornershop.counterstest.data.mappers.toSelectableCounterList
import com.cornershop.counterstest.data.utils.getPersonalizedMessage
import com.cornershop.counterstest.data.worker.SyncDataWorker
import com.cornershop.counterstest.data.worker.SyncDataWorker.Companion.createRequest
import com.cornershop.counterstest.data.worker.SyncDataWorker.SyncTask.*
import com.cornershop.counterstest.domain.response.Counter
import com.cornershop.counterstest.domain.response.SelectableCounter
import com.cornershop.counterstest.usecases.usecases.local.*
import com.cornershop.counterstest.usecases.usecases.server.RemoteGetCountersUseCase
import com.cornershop.counterstest.usecases.usecases.server.RemoteSaveCounterUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CountersViewModel @Inject constructor(
    private val remoteSaveCounterUseCase: RemoteSaveCounterUseCase,
    private val remoteGetCountersUseCase: RemoteGetCountersUseCase,
    private val localDecrementCounterUseCase: LocalDecrementCounterUseCase,
    private val localDeleteCounterUseCase: LocalDeleteCounterUseCase,
    private val localGetCountersUseCase: LocalGetCountersUseCase,
    private val localIncrementCounterUseCase: LocalIncrementCounterUseCase,
    private val localSaveCounterUseCase: LocalSaveCounterUseCase,
    private val workManager: WorkManager
) : ViewModel() {
    private val _liveData = MutableLiveData<Response<Pair<Int, List<SelectableCounter>>>>()
    val liveData: LiveData<Response<Pair<Int, List<SelectableCounter>>>>
        get() = _liveData

    fun incrementCounter(counter: Counter) {
        viewModelScope.launch {
            _liveData.value = Response.Loading()
            localIncrementCounterUseCase.invoke(counter)
            workManager.enqueue(
                createRequest(
                    workDataOf(
                        SyncDataWorker.SYNC_TASK to INCREMENT_COUNTER.name,
                        SyncDataWorker.ID to counter.id
                    )
                )
            )
            getCounters()
        }
    }

    fun decrementCounter(counter: Counter) {
        viewModelScope.launch {
            _liveData.value = Response.Loading()
            localDecrementCounterUseCase.invoke(counter)
            workManager.enqueue(
                createRequest(
                    workDataOf(
                        SyncDataWorker.SYNC_TASK to DECREMENT_COUNTER.name,
                        SyncDataWorker.ID to counter.id
                    )
                )
            )
            getCounters()
        }
    }

    fun delete(counters: List<SelectableCounter?>?) = viewModelScope.launch {
        _liveData.value = Response.Loading()
        val list = mutableListOf<OneTimeWorkRequest>()
        counters?.forEach { counter ->
            if (counter != null) {
                localDeleteCounterUseCase.invoke(counter.toCounter())
                (createRequest(
                    workDataOf(
                        SyncDataWorker.SYNC_TASK to DELETE_COUNTER.name,
                        SyncDataWorker.ID to counter.id
                    )
                ) as? OneTimeWorkRequest)?.let {
                    list.add(it)
                }
            }
        }
        workManager.enqueue(list)
        getCounters()
    }

    fun getCounters(firstLoad: Boolean = false) {
        viewModelScope.launch {
            if (firstLoad)
                _liveData.value = Response.Loading()
            val localCounters = localGetCountersUseCase.invoke().sortedBy { it.title }
            val selectableCounterList = if (localCounters.isEmpty() && firstLoad) {
                val remoteCounters = remoteGetCountersUseCase.invoke()
                remoteCounters.forEach { remoteCounter ->
                    localSaveCounterUseCase.invoke(remoteCounter)
                }
                localGetCountersUseCase.invoke().sortedBy { it.title }.toSelectableCounterList()
            } else
                localCounters.toSelectableCounterList()

            val count = selectableCounterList.sumBy { it.count }
            _liveData.value =
                Response.Success(count to selectableCounterList)
        }
    }

    fun saveCounter(title: String) {
        viewModelScope.launch {
            _liveData.value = Response.Loading()
            remoteSaveCounterUseCase.invoke(title).asFlow().flowOn(IO).catch { error ->
                _liveData.value = Response.Error(error.getPersonalizedMessage())
            }.collect { data ->
                saveIntoLocal(data)
            }
        }
    }

    private fun saveIntoLocal(data: Counter) {
        viewModelScope.launch {
            localSaveCounterUseCase.invoke(data).asFlow().flowOn(IO).catch { error ->
                _liveData.value = Response.Error(error.getPersonalizedMessage())
            }.collect { data ->
                getCounters()
            }
        }
    }
}