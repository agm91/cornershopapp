package com.cornershop.counterstest.presentation.viewmodels

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkInfo
import android.net.NetworkRequest
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
@HiltViewModel
class NetworkMonitoringViewModel @Inject constructor(
    private val applicationContext: Application,
    private val networkRequest: NetworkRequest
) : AndroidViewModel(applicationContext) {
    private val connectivityManager =
        applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    private val _liveData = MutableLiveData<Boolean>()
    val liveData: LiveData<Boolean>
        get() = _liveData

    private val callback = object : ConnectivityManager.NetworkCallback() {
        override fun onAvailable(network: Network) {
            viewModelScope.launch { withContext(Main) { _liveData.value = true } }
        }

        override fun onLost(network: Network) {
            viewModelScope.launch { withContext(Main) { _liveData.value = false } }
        }
    }

    fun enable() {
        connectivityManager.registerNetworkCallback(networkRequest, callback)
        initialState()
    }

    private fun initialState() = viewModelScope.launch {
        withContext(Main) {
            val activeNetwork: NetworkInfo? = connectivityManager.activeNetworkInfo
            _liveData.value = activeNetwork?.isConnected == true
        }
    }

    private fun disable() = connectivityManager.unregisterNetworkCallback(callback)


    override fun onCleared() {
        super.onCleared()
        disable()
    }
}