package com.cornershop.counterstest.presentation

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.cornershop.counterstest.R
import com.cornershop.counterstest.databinding.ActivityMainBinding
import com.cornershop.counterstest.presentation.viewmodels.NetworkMonitoringViewModel
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    private val monitoringViewModel: NetworkMonitoringViewModel by viewModels()

    private lateinit var snackBar: Snackbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        monitoringViewModel.enable()

        snackBar = Snackbar.make(
            binding.root,
            getString(R.string.connection_error_description),
            Snackbar.LENGTH_INDEFINITE
        )

        monitoringViewModel.liveData.observe(this, { hasInternet ->
            if (hasInternet) snackBar.dismiss()
            else snackBar.show()
        })
    }
}
