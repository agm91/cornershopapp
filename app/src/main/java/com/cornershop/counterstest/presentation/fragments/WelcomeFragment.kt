package com.cornershop.counterstest.presentation.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.NavHostFragment.findNavController
import com.cornershop.counterstest.R
import com.cornershop.counterstest.data.datastore.CornerDataStore
import com.cornershop.counterstest.databinding.FragmentWelcomeBinding
import com.cornershop.counterstest.presentation.common.navigateSafely
import com.cornershop.counterstest.presentation.common.show
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@AndroidEntryPoint
class WelcomeFragment : Fragment() {
    private lateinit var binding: FragmentWelcomeBinding

    @Inject
    lateinit var cornerDataStore: CornerDataStore

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_welcome, container, false
        )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lifecycleScope.launch { shouldRedirectToWelcome() }
        binding.itemWelcome?.buttonStart?.setOnClickListener { onWelcomeClicked() }
    }

    private suspend fun shouldRedirectToWelcome() {
        cornerDataStore.hasShownWelcome.collect { hasShown ->
            if (hasShown)
                findNavController(this)
                    .navigate(WelcomeFragmentDirections.actionWelcomeFragmentToCountersFragment())
            else binding.layout?.show()
        }
    }

    private fun onWelcomeClicked() {
        lifecycleScope.launch {
            cornerDataStore.saveToDataStore(true)
            withContext(Main) { navigateToCounters() }
        }
    }

    private fun navigateToCounters() = findNavController(this).navigateSafely(
        WelcomeFragmentDirections.actionWelcomeFragmentToCountersFragment()
    )

    companion object {
        @JvmStatic
        fun newInstance() = WelcomeFragment()
    }
}