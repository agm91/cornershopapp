package com.cornershop.counterstest.presentation.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.cornershop.counterstest.R
import com.cornershop.counterstest.data.mappers.toCounter
import com.cornershop.counterstest.databinding.ItemCountersBinding
import com.cornershop.counterstest.domain.response.Counter
import com.cornershop.counterstest.domain.response.SelectableCounter
import com.cornershop.counterstest.presentation.common.select
import com.cornershop.counterstest.presentation.common.unselect
import javax.inject.Inject

class CountersAdapter @Inject constructor() :
    ListAdapter<SelectableCounter, CountersAdapter.ViewHolder>(CountersDiffCallback()),
    Filterable {
    private lateinit var binding: ItemCountersBinding

    var list: List<SelectableCounter>? = null
    private var filteredList: List<SelectableCounter>? = null

    val selectedMap = mutableMapOf<Int, SelectableCounter?>()
    var listener: OnCounterClickListener? = null

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val counter = getItem(position)
        holder.bind(counter)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        binding = DataBindingUtil.inflate(
            layoutInflater, R.layout.item_counters, parent, false
        )
        return ViewHolder(binding)
    }

    inner class ViewHolder(
        private val binding: ItemCountersBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(counter: SelectableCounter) {
            with(binding) {
                textviewCounterName.text = counter.title
                textviewCounterAmount.text = counter.count.toString()
                buttonPlus.setOnClickListener { listener?.onIncrement(counter.toCounter()) }
                setMinusButton(counter)
                if (counter.isSelected) select() else unselect()
                root.setOnLongClickListener { onLongClick(counter) }
                root.setOnClickListener { onClick(counter) }
            }
        }

        private fun setMinusButton(counter: SelectableCounter) {
            with(binding) {
                if (counter.count <= 0) {
                    buttonMinus.setImageDrawable(
                        ContextCompat.getDrawable(root.context, R.drawable.ic_minus_disabled)
                    )
                    buttonMinus.setOnClickListener(null)
                } else {
                    buttonMinus.setImageDrawable(
                        ContextCompat.getDrawable(root.context, R.drawable.ic_minus)
                    )
                    buttonMinus.setOnClickListener { listener?.onDecrement(counter.toCounter()) }
                }
            }
        }

        private fun onClick(counter: SelectableCounter) {
            if (selectedMap.isNotEmpty()) {
                if (selectedMap[adapterPosition] != null) {
                    binding.unselect()
                    selectedMap.remove(adapterPosition)
                } else if (selectedMap.isNotEmpty()) {
                    binding.select()
                    selectedMap[adapterPosition] = counter
                    listener?.selected(selectedMap.size)
                }
                if (selectedMap.isEmpty())
                    listener?.deactivateMultipleSelection()
            }
        }

        private fun onLongClick(counter: SelectableCounter): Boolean {
            if (selectedMap.isEmpty())
                listener?.activateMultipleSelection()
            if (selectedMap[adapterPosition] != null) {
                binding.unselect()
                selectedMap.remove(adapterPosition)
            } else {
                binding.select()
                selectedMap[adapterPosition] = counter
            }
            listener?.selected(selectedMap.size)
            if (selectedMap.isEmpty())
                listener?.deactivateMultipleSelection()
            return true
        }
    }

    interface OnCounterClickListener {
        fun activateMultipleSelection()
        fun deactivateMultipleSelection()
        fun getCountersInfo(times: Int?, list: List<SelectableCounter>?)
        fun selected(amount: Int)
        fun onIncrement(counter: Counter)
        fun onDecrement(counter: Counter)
    }

    class CountersDiffCallback : DiffUtil.ItemCallback<SelectableCounter>() {
        override fun areItemsTheSame(
            oldItem: SelectableCounter, newItem: SelectableCounter
        ) = oldItem.id == newItem.id

        override fun areContentsTheSame(
            oldItem: SelectableCounter, newItem: SelectableCounter
        ) = oldItem == newItem
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {
                if (charSequence.isEmpty()) {
                    filteredList = list
                } else {
                    val filteredList = list?.filter { counter ->
                        counter.title.startsWith(charSequence)
                    }
                    this@CountersAdapter.filteredList = filteredList
                }
                val filterResults = FilterResults()
                filterResults.values = filteredList
                return filterResults
            }

            override fun publishResults(
                charSequence: CharSequence,
                filterResults: FilterResults
            ) {
                filteredList = filterResults.values as? List<SelectableCounter>
                val count = filteredList?.sumBy { it.count }
                listener?.getCountersInfo(count, filteredList)
                submitList(filteredList)
            }
        }
    }
}