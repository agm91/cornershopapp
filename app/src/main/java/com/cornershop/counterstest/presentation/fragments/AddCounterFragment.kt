package com.cornershop.counterstest.presentation.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.addTextChangedListener
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.NavHostFragment.findNavController
import com.cornershop.counterstest.R
import com.cornershop.counterstest.data.utils.getMessage
import com.cornershop.counterstest.data.utils.handleResponse
import com.cornershop.counterstest.databinding.FragmentAddCounterBinding
import com.cornershop.counterstest.presentation.common.gone
import com.cornershop.counterstest.presentation.common.show
import com.cornershop.counterstest.presentation.common.showSnackBar
import com.cornershop.counterstest.presentation.dialog.SingleButtonDialog
import com.cornershop.counterstest.presentation.viewmodels.CountersViewModel
import com.cornershop.counterstest.presentation.viewmodels.NetworkMonitoringViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AddCounterFragment : Fragment() {
    private lateinit var binding: FragmentAddCounterBinding

    private val monitoringViewModel: NetworkMonitoringViewModel by activityViewModels()
    private val viewModel: CountersViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_add_counter, container, false
        )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
    }

    private fun observeForSaveLocal() {
        viewModel.liveData.observe(viewLifecycleOwner, { response ->
            response.handleResponse(
                binding.layoutProgress.progressBar,
                whenSuccess = {
                    findNavController(this).navigateUp()
                },
                whenError = { messageHandler ->
                    binding.textviewAction.show()
                    binding.root.showSnackBar(messageHandler.getMessage(requireContext()))
                })
        })
    }

    private fun initView() {
        with(binding) {
            textviewTitle.text = getString(R.string.create_counter)
            textviewAction.text = getString(R.string.save)
            textviewAction.setOnClickListener { onAddCounterButtonClicked() }
            texteditCounterName.addTextChangedListener { text ->
                if (text?.isEmpty() == true) textviewAction.gone()
                else textviewAction.show()
            }
            textviewAction.gone()
            buttonCancel.apply {
                show()
                setOnClickListener { findNavController(this@AddCounterFragment).navigateUp() }
            }
        }
    }

    private fun onAddCounterButtonClicked() {
        if (monitoringViewModel.liveData.value == false) {
            SingleButtonDialog.newInstance(
                getString(R.string.error_deleting_counter_title),
                getString(R.string.connection_error_description),
                getString(android.R.string.ok)
            ).show(parentFragmentManager, "no_internet_dialog")
        } else {
            observeForSaveLocal()
            viewModel.saveCounter(binding.texteditCounterName.text.toString())
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() = AddCounterFragment()
    }
}