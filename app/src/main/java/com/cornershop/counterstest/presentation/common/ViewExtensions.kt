package com.cornershop.counterstest.presentation.common

import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.graphics.BlendModeColorFilterCompat
import androidx.core.graphics.BlendModeCompat
import com.cornershop.counterstest.R
import com.cornershop.counterstest.databinding.ItemCountersBinding
import com.google.android.material.snackbar.Snackbar

fun View.gone() {
    this.visibility = View.GONE
}

fun View.show() {
    this.visibility = View.VISIBLE
}

fun View.invisible() {
    this.visibility = View.INVISIBLE
}

fun View.showSnackBar(text: String) = Snackbar.make(this, text, Snackbar.LENGTH_SHORT).show()

fun ItemCountersBinding.unselect() {
    with(this) {
        val drawable = ContextCompat.getDrawable(
            root.context, R.drawable.drawable_counter_background
        )
        layoutCounter.background = drawable
        imageViewCheck.gone()
        groupUnselected.show()
    }
}

fun ItemCountersBinding.select() {
    with(this) {
        val drawable = ContextCompat.getDrawable(
            this.root.context,
            R.drawable.drawable_counter_background
        )
        val secondaryColor = ContextCompat.getColor(this.root.context, R.color.orange_light)
        drawable?.colorFilter =
            BlendModeColorFilterCompat.createBlendModeColorFilterCompat(
                secondaryColor,
                BlendModeCompat.SRC_ATOP
            )
        layoutCounter.background = drawable
        imageViewCheck.show()
        groupUnselected.gone()
    }
}