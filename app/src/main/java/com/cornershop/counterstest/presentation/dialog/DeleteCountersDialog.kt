package com.cornershop.counterstest.presentation.dialog

import android.app.AlertDialog
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.cornershop.counterstest.R
import com.cornershop.counterstest.databinding.DialogTwoButtonsBinding
import com.cornershop.counterstest.presentation.common.show
import com.cornershop.counterstest.presentation.fragments.CountersFragment

class DeleteCountersDialog : DialogFragment() {
    private lateinit var binding: DialogTwoButtonsBinding

    private var title: String? = null
    private var description: String? = null
    private var buttonPositiveText: String? = null
    private var buttonNegativeText: String? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = AlertDialog.Builder(requireActivity())
        dialog.setCancelable(false)
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.dialog_two_buttons,
            null,
            false
        )
        initDialog()
        dialog.setView(binding.root)
        this@DeleteCountersDialog.dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        return dialog.create()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog?.window?.requestFeature(Window.FEATURE_NO_TITLE)
        return binding.root
    }

    private fun initDialog() {
        with(binding) {
            textViewTitle.text = title
            textViewContent.text = description
            textViewActionPositive.text = buttonPositiveText
            textViewActionPositive.setOnClickListener {
                parentFragmentManager.setFragmentResult(
                    CountersFragment.ACTION_DELETE, bundleOf(DELETE_SELECTED to true)
                )
                this@DeleteCountersDialog.dismiss()
            }
            textViewActionNegative.apply {
                show()
                text = buttonNegativeText
                setOnClickListener { this@DeleteCountersDialog.dismiss() }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        title = arguments?.getString(TITLE)
        description = arguments?.getString(DESCRIPTION)
        buttonPositiveText = arguments?.getString(BUTTON_POSITIVE_TEXT)
        buttonNegativeText = arguments?.getString(BUTTON_NEGATIVE_TEXT)
    }

    companion object {
        private const val TITLE = "ip"
        private const val DESCRIPTION = "description"
        private const val BUTTON_POSITIVE_TEXT = "button_positive_text"
        private const val BUTTON_NEGATIVE_TEXT = "button_negative_text"
        const val DELETE_SELECTED = "delete_selected"

        @JvmStatic
        fun newInstance(
            title: String,
            description: String,
            buttonPositiveText: String,
            buttonNegativeText: String
        ): DeleteCountersDialog {
            val fragment = DeleteCountersDialog()
            val args = Bundle().apply {
                putString(TITLE, title)
                putString(DESCRIPTION, description)
                putString(BUTTON_POSITIVE_TEXT, buttonPositiveText)
                putString(BUTTON_NEGATIVE_TEXT, buttonNegativeText)
            }
            fragment.arguments = args
            return fragment
        }
    }
}