package com.cornershop.counterstest.usecases.usecases.local

import com.cornershop.counterstest.data.repository.LocalCounterRepository
import com.cornershop.counterstest.domain.response.Counter

class LocalGetCountersUseCase(private val localCounterRepository: LocalCounterRepository) {
    suspend fun invoke(): List<Counter> = localCounterRepository.getCounters()
}