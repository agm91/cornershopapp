package com.cornershop.counterstest.usecases.usecases.local

import com.cornershop.counterstest.data.repository.LocalCounterRepository
import com.cornershop.counterstest.domain.response.Counter

class LocalIncrementCounterUseCase(private val localCounterRepository: LocalCounterRepository) {
    suspend fun invoke(counter: Counter): List<Counter> =
        localCounterRepository.incrementCounter(counter)
}