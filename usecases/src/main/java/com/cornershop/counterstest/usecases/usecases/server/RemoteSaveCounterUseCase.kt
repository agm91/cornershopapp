package com.cornershop.counterstest.usecases.usecases.server

import com.cornershop.counterstest.data.repository.RemoteCounterRepository
import com.cornershop.counterstest.domain.response.Counter

class RemoteSaveCounterUseCase(private val remoteCounterRepository: RemoteCounterRepository) {
    suspend fun invoke(title: String): List<Counter> =
        remoteCounterRepository.saveCounter(title)
}