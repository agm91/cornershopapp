package com.cornershop.counterstest.usecases.usecases.server

import com.cornershop.counterstest.data.repository.RemoteCounterRepository
import com.cornershop.counterstest.domain.response.Counter

class RemoteGetCountersUseCase(private val remoteCounterRepository: RemoteCounterRepository) {
    suspend fun invoke(): List<Counter> = remoteCounterRepository.getCounters()
}