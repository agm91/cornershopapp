package com.cornershop.counterstest.usecases.usecases.local

import com.cornershop.counterstest.data.repository.LocalCounterRepository
import com.cornershop.counterstest.testShared.mockedCounter
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class LocalDecrementCounterUseCaseTest {

    @Mock
    lateinit var localCounterRepository: LocalCounterRepository

    lateinit var localDecrementCounterUseCase: LocalDecrementCounterUseCase

    @Before
    fun setUp() {
        localDecrementCounterUseCase = LocalDecrementCounterUseCase(localCounterRepository)
    }

    @Test
    fun `invoke calls local repository`() {
        runBlocking {
            val counter = mockedCounter.copy(id = "123", count = 2)
            whenever(localCounterRepository.decrementCounter(counter))
                .thenReturn(listOf(counter.copy(count = 1)))
            val result = localDecrementCounterUseCase.invoke(counter)
            assertEquals(listOf(counter.copy(count = 1)), result)
        }
    }
}