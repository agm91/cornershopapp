package com.cornershop.counterstest.usecases.usecases.server

import com.cornershop.counterstest.data.repository.RemoteCounterRepository
import com.cornershop.counterstest.testShared.mockedCounter
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*

import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class RemoteSaveCounterUseCaseTest {

    @Mock
    lateinit var remoteCounterRepository: RemoteCounterRepository

    lateinit var remoteSaveCounterUseCase: RemoteSaveCounterUseCase

    @Before
    fun setUp() {
        remoteSaveCounterUseCase = RemoteSaveCounterUseCase(remoteCounterRepository)
    }

    @Test
    fun `invoke calls local repository`() {
        runBlocking {
            val counter = mockedCounter.copy(id = "123", "Test!")
            whenever(remoteCounterRepository.saveCounter("Test!"))
                .thenReturn(listOf(counter))
            val result = remoteSaveCounterUseCase.invoke("Test!")
            assertEquals(listOf(counter), result)
        }
    }
}