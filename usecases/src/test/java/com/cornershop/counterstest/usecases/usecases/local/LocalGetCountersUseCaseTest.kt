package com.cornershop.counterstest.usecases.usecases.local

import com.cornershop.counterstest.data.repository.LocalCounterRepository
import com.cornershop.counterstest.testShared.mockedCounter
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class LocalGetCountersUseCaseTest {

    @Mock
    lateinit var localCounterRepository: LocalCounterRepository

    lateinit var localGetCountersUseCase: LocalGetCountersUseCase

    @Before
    fun setUp() {
        localGetCountersUseCase = LocalGetCountersUseCase(localCounterRepository)
    }

    @Test
    fun `invoke calls local repository`() {
        runBlocking {
            val counter = mockedCounter.copy(id = "123", count = 2)
            whenever(localCounterRepository.getCounters())
                .thenReturn(listOf(counter))
            val result = localGetCountersUseCase.invoke()
            assertEquals(listOf(counter), result)
        }
    }
}