package com.cornershop.counterstest.usecases.usecases.server

import com.cornershop.counterstest.data.repository.RemoteCounterRepository
import com.cornershop.counterstest.data.source.RemoteDataSource
import com.cornershop.counterstest.testShared.mockedCounter
import com.cornershop.counterstest.usecases.usecases.local.LocalSaveCounterUseCase
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.runBlocking
import org.junit.Assert.*

import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.w3c.dom.css.Counter

@RunWith(MockitoJUnitRunner::class)
class RemoteGetCountersUseCaseTest {
    @Mock
    lateinit var remoteCounterRepository: RemoteCounterRepository

    lateinit var remoteGetCountersUseCase: RemoteGetCountersUseCase

    @Before
    fun setUp() {
        remoteGetCountersUseCase = RemoteGetCountersUseCase(remoteCounterRepository)
    }

    @Test
    fun `invoke calls local repository`() {
        runBlocking {
            val counter = mockedCounter.copy(id = "123")
            whenever(remoteCounterRepository.getCounters())
                .thenReturn(listOf(counter))
            val result = remoteGetCountersUseCase.invoke()
            assertEquals(listOf(counter), result)
        }
    }
}