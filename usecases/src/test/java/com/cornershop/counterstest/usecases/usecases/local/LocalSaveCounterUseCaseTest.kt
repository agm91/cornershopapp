package com.cornershop.counterstest.usecases.usecases.local

import com.cornershop.counterstest.data.repository.LocalCounterRepository
import com.cornershop.counterstest.testShared.mockedCounter
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import org.w3c.dom.css.Counter

@RunWith(MockitoJUnitRunner::class)
class LocalSaveCounterUseCaseTest {

    @Mock
    lateinit var localCounterRepository: LocalCounterRepository

    lateinit var localSaveCounterUseCase: LocalSaveCounterUseCase

    @Before
    fun setUp() {
        localSaveCounterUseCase = LocalSaveCounterUseCase(localCounterRepository)
    }

    @Test
    fun `invoke calls local repository`() {
        runBlocking {
            val counter = mockedCounter.copy(id = "123")
            whenever(localCounterRepository.saveCounter(counter))
                .thenReturn(listOf())
            val result = localSaveCounterUseCase.invoke(counter)
            assertEquals(listOf<Counter>(), result)
        }
    }
}