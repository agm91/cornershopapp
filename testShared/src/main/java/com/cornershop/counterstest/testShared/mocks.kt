package com.cornershop.counterstest.testShared

import com.cornershop.counterstest.domain.response.Counter
import com.cornershop.counterstest.domain.response.SelectableCounter

val mockedCounter = Counter("0", "Title", 0)

val mockedSelectableCounter = SelectableCounter(false, "0", "Title", 0)