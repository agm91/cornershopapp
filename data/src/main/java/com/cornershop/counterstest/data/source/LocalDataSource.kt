package com.cornershop.counterstest.data.source

import com.cornershop.counterstest.domain.response.Counter

interface LocalDataSource {
    suspend fun getCounters(): List<Counter>
    suspend fun saveCounter(counter: Counter): List<Counter>
    suspend fun incrementCounter(counter: Counter): List<Counter>
    suspend fun counterCount(): Int
    suspend fun decrementCounter(counter: Counter): List<Counter>
    suspend fun deleteCounter(counter: Counter): List<Counter>
}