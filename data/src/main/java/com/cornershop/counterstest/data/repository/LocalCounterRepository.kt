package com.cornershop.counterstest.data.repository

import com.cornershop.counterstest.data.source.LocalDataSource
import com.cornershop.counterstest.domain.response.Counter

class LocalCounterRepository(private val localDataSource: LocalDataSource) {
    suspend fun getCounters(): List<Counter> = localDataSource.getCounters()

    suspend fun saveCounter(counter: Counter) = localDataSource.saveCounter(counter)

    suspend fun incrementCounter(counter: Counter): List<Counter> =
        localDataSource.incrementCounter(counter)

    suspend fun decrementCounter(counter: Counter): List<Counter> =
        localDataSource.decrementCounter(counter)

    suspend fun deleteCounter(counter: Counter): List<Counter> =
        localDataSource.deleteCounter(counter)

    suspend fun counterCount(): Int =
        localDataSource.counterCount()
}