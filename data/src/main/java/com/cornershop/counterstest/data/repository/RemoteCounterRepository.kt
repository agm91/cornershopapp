package com.cornershop.counterstest.data.repository

import com.cornershop.counterstest.data.source.RemoteDataSource
import com.cornershop.counterstest.domain.response.Counter

class RemoteCounterRepository(private val remoteDataSource: RemoteDataSource) {
    suspend fun getCounters(): List<Counter> =
        remoteDataSource.getCounters()

    suspend fun saveCounter(title: String): List<Counter> =
        remoteDataSource.saveCounter(title)

    suspend fun incrementCounter(id: String): List<Counter> =
        remoteDataSource.incrementCounter(id)

    suspend fun decrementCounter(id: String): List<Counter> =
        remoteDataSource.decrementCounter(id)

    suspend fun deleteCounter(id: String): List<Counter> =
        remoteDataSource.deleteCounter(id)
}