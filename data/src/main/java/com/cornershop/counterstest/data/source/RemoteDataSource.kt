package com.cornershop.counterstest.data.source

import com.cornershop.counterstest.domain.response.Counter

interface RemoteDataSource {
    suspend fun getCounters(): List<Counter>
    suspend fun saveCounter(title: String): List<Counter>
    suspend fun incrementCounter(id: String): List<Counter>
    suspend fun decrementCounter(id: String): List<Counter>
    suspend fun deleteCounter(id: String): List<Counter>
}