package com.cornershop.counterstest.data.repository

import com.cornershop.counterstest.data.source.LocalDataSource
import com.cornershop.counterstest.domain.response.Counter
import com.cornershop.counterstest.testShared.mockedCounter
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class LocalCounterRepositoryTest {

    private lateinit var localCounterRepository: LocalCounterRepository

    @Mock
    lateinit var localDataSource: LocalDataSource

    @Before
    fun setUp() {
        localCounterRepository = LocalCounterRepository(localDataSource)
    }

    @Test
    fun `counters are saved correctly`() {
        runBlocking {
            val counter = mockedCounter.copy(id = "1", count = 1)
            whenever(localDataSource.saveCounter(counter)).thenReturn(listOf(counter))
            whenever(localDataSource.getCounters()).thenReturn(listOf(counter))
            localCounterRepository.saveCounter(counter)
            val counters = localCounterRepository.getCounters()
            assertEquals(listOf(counter), counters)
            verify(localDataSource).getCounters()
            verify(localDataSource).saveCounter(counter)
        }
    }

    @Test
    fun `counters are deleted correctly`() {
        runBlocking {
            val counter = mockedCounter.copy(id = "1", count = 1)
            whenever(localDataSource.saveCounter(counter)).thenReturn(listOf(counter))
            whenever(localDataSource.getCounters()).thenReturn(listOf(counter))
            whenever(localDataSource.deleteCounter(counter)).thenReturn(listOf())
            localCounterRepository.saveCounter(counter)
            val counters = localCounterRepository.getCounters()
            assertEquals(listOf(counter), counters)
            assertEquals(listOf<Counter>(), localCounterRepository.deleteCounter(counter))
            verify(localDataSource).getCounters()
            verify(localDataSource).saveCounter(counter)
            verify(localDataSource).deleteCounter(counter)
        }
    }

    @Test
    fun `increment function is correctly set counter + 1`() {
        runBlocking {
            val counter = mockedCounter.copy(count = 0)
            whenever(localDataSource.incrementCounter(counter))
                .thenReturn(listOf(mockedCounter.copy(count = 1)))
            val counters = localCounterRepository.incrementCounter(counter)
            assertEquals(listOf(mockedCounter.copy(count = 1)), counters)
        }
    }

    @Test
    fun `decrement function is correctly set counter - 1`() {
        runBlocking {
            val counter = mockedCounter.copy(count = 1)
            whenever(localDataSource.decrementCounter(counter))
                .thenReturn(listOf(mockedCounter.copy(count = 0)))
            val counters = localCounterRepository.decrementCounter(counter)
            assertEquals(listOf(mockedCounter.copy(count = 0)), counters)
        }
    }
}