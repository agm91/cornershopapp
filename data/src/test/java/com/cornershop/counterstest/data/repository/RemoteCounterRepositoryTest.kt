package com.cornershop.counterstest.data.repository

import com.cornershop.counterstest.data.source.RemoteDataSource
import com.cornershop.counterstest.domain.response.Counter
import com.cornershop.counterstest.testShared.mockedCounter
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class RemoteCounterRepositoryTest {

    private lateinit var remoteCounterRepository: RemoteCounterRepository

    @Mock
    lateinit var remoteDataSource: RemoteDataSource

    @Before
    fun setUp() {
        remoteCounterRepository = RemoteCounterRepository(remoteDataSource)
    }

    @Test
    fun `counters are saved correctly`() {
        runBlocking {
            val counter = mockedCounter.copy(id = "123", title = "Test", count = 0)
            whenever(remoteDataSource.saveCounter("Test")).thenReturn(listOf(counter))
            whenever(remoteDataSource.getCounters()).thenReturn(listOf(counter))
            remoteCounterRepository.saveCounter("Test")
            val counters = remoteCounterRepository.getCounters()
            assertEquals(listOf(counter), counters)
            verify(remoteDataSource).getCounters()
            verify(remoteDataSource).saveCounter("Test")
        }
    }

    @Test
    fun `counters are deleted correctly`() {
        runBlocking {
            val counter = mockedCounter.copy(id = "123", title = "Test", count = 0)
            whenever(remoteDataSource.saveCounter("Test")).thenReturn(listOf(counter))
            whenever(remoteDataSource.getCounters()).thenReturn(listOf(counter))
            whenever(remoteDataSource.deleteCounter("123")).thenReturn(listOf())
            remoteCounterRepository.saveCounter("Test")
            val counters = remoteCounterRepository.getCounters()
            assertEquals(listOf(counter), counters)
            assertEquals(listOf<Counter>(), remoteCounterRepository.deleteCounter("123"))
            verify(remoteDataSource).getCounters()
            verify(remoteDataSource).saveCounter("Test")
            verify(remoteDataSource).deleteCounter("123")
        }
    }

    @Test
    fun `increment function is correctly set counter + 1`() {
        runBlocking {
            whenever(remoteDataSource.incrementCounter("123"))
                .thenReturn(listOf(mockedCounter.copy(count = 1)))
            val counters = remoteCounterRepository.incrementCounter("123")
            assertEquals(listOf(mockedCounter.copy(count = 1)), counters)
        }
    }

    @Test
    fun `decrement function is correctly set counter - 1`() {
        runBlocking {
            whenever(remoteDataSource.decrementCounter("123"))
                .thenReturn(listOf(mockedCounter.copy(count = 0)))
            val counters = remoteCounterRepository.decrementCounter("123")
            assertEquals(listOf(mockedCounter.copy(count = 0)), counters)
        }
    }
}