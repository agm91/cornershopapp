package com.cornershop.counterstest.domain.entity

data class TitleEntity(val title: String)