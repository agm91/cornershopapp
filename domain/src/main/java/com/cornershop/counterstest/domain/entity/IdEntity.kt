package com.cornershop.counterstest.domain.entity

data class IdEntity(val id: String)