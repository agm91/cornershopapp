package com.cornershop.counterstest.domain.response

data class SelectableCounter(
    var isSelected: Boolean = false,
    val id: String,
    val title: String,
    val count: Int
)