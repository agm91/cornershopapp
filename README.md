# README #

You can clone this project in order to see and compile code and tests.

Also, please, proceed and download [this link](https://www.dropbox.com/sh/dqef0h5czcvlsg7/AAB1oKweZefkf8L5A6WM-V2la?dl=0) the project named server. (I did not upload it here because I do not own it)


### You will need *.properties ###

* debug and release properties extension files
* Each should contain base_url

### How do I get set up? ###

* Once cloned and containing the debug and release properties, sync gradle files.
* Open a terminal, go to the server project location and type npm start
* On Android Studio, set build variant: Debug.
* Get the local ip address of the computer running the server and paste it on base_url on debug.properties. Should look like: http://192.168.68.103:3000/

### Architecture ###

* Clean
* MVVM

### Offline mode ###

* For creating counters, Internet is a must
* Once created, you can edit it without internet. The Work should upload the changes when internet gets back.

### Who do I talk to? ###

* Alejandro Gonzalez Martinez

Thank you again for the oportunity!